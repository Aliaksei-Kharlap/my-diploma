from django import template
from mykino.models import Genre, Movie
import requests
from bs4 import BeautifulSoup

register = template.Library()


@register.simple_tag()
def get_categories():
    """Вывод всех категорий"""
    return Genre.objects.all()


@register.inclusion_tag('movies/tags/last_movies.html')
def get_last_movies(count=2):
    movies = Movie.objects.order_by("id")[:count]
    return {"last_movies": movies}

@register.simple_tag()
def parsingpage():
    DOLLAR_BYN = "https://myfin.by/currency/minsk"
    headers = {"User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0"}

    full_page = requests.get(DOLLAR_BYN, headers=headers)
    dad = BeautifulSoup(full_page.content, "html.parser")
    convert = dad.find("div", {"class": "bl_usd_ex", "class": "curr_block"})
    cours = convert.text
    return cours