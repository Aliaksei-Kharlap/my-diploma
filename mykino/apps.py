from django.apps import AppConfig


class MykinoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mykino'
    verbose_name = "Фильмы"