from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView

from django.views.generic.base import View
from .forms import ReviewForm, RatingForm
from .models import Movie, Genre, Actor, Rating
import requests
from bs4 import BeautifulSoup


class GenreYear:
    """Жанры и года выхода фильмов"""
    def get_genres(self):
        return Genre.objects.all()

    def get_years(self):
        return Movie.objects.filter(premium=False).values("year")

    # def parsingpage(self):
    #     DOLLAR_BYN = "https://www.google.com/search?q=%D0%BA%D1%83%D1%80%D1%81%D1%8B+%D0%B4%D0%BE%D0%BB%D0%BB%D0%B0%D1%80%D0%B0+%D0%BA+%D0%B1%D0%B5%D0%BB%D0%BE%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%BE%D0%BC%D1%83+%D1%80%D1%83%D0%B1%D0%BB%D1%8E&oq=%D0%BA%D1%83%D1%80%D1%81%D1%8B+%D0%B4%D0%BE%D0%BB%D0%BB%D0%B0%D1%80%D0%B0+%D0%BA&aqs=chrome.3.69i57j0i512l9.13088j1j1&sourceid=chrome&ie=UTF-8"
    #     headerss = {
    #         "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36"}
    #     full_page = requests.get(DOLLAR_BYN, headerss)
    #     soup = BeautifulSoup(full_page.content, "html.parser")
    #     convert = soup.findAll("span", {"class": "DFlfde", "class": "SwHCTb", "data-precision": 2})
    #     cours = convert[0].text
    #     return cours

class MovieView(GenreYear, ListView):

    model = Movie
    queryset = Movie.objects.filter(premium = False)
    template_name = "movies/movie_list.html"
    paginate_by = 6

class MovieDetailView(GenreYear, DetailView):
    """Полное описание фильма"""
    model = Movie
    slug_field = "url"
    template_name = "movies/movie_detail.html"
    # def get (self, request, slug):
    #     movie = Movie.objects.get(url=slug)
    #     return render(request, "movies/movie_detail.html", {"movie": movie})
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["star_form"] = RatingForm()
        return context

class AddReview(View):
    def post(self, request, pk):
        form = ReviewForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.movie = Movie.objects.get(id=pk)
            form.user = request.user
            form.save()
        return redirect(Movie.objects.get(id=pk).get_absolute_url())

class ActorView(GenreYear, DetailView):
    model = Actor
    template_name = "movies/actor.html"
    slug_field = "name"


def filer_movie(request):
    queryset = Movie.objects.filter(
        Q(year__in=request.GET.getlist("year")) |
        Q(genres__in=request.GET.getlist("genre"))
    )
    return render(request, "movies/movie_list.html", {'movie_list': queryset})

class AddStarRating(View):
    """Добавление рейтинга фильму"""

    def post(self, request):
        form = RatingForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.movie.id = request.POST.get("movie")
            form.user = request.user
            form.save()
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=400)

# class Search(ListView):
#     """Поиск фильмов"""
#     paginate_by = 3
#
#     def get_queryset(self):
#         return Movie.objects.filter(title__icontains=self.request.GET.get("q"))
#
#     def get_context_data(self, *args, **kwargs):
#         context = super().get_context_data(*args, **kwargs)
#         context["q"] = f'q={self.request.GET.get("q")}&'
#         return context

def search(request, *args, **kwargs):
    context =Movie.objects.filter(title__icontains=request.GET.get("q"))
    return render(request, "movies/movie_list.html", {'movie_list': context})


